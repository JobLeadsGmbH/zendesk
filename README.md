# Zend Framework 2 Module for ZendDesk

This Module is a thin wrapper to easy up the usage of the official [zendesk client](https://github.com/zendesk/zendesk_api_client_php) php client.

# Installation

## By Hand

```
mkdir -p vendor/JobLeadsGmbH/zendesk
cd vendor/JobLeadsGmbH/zendesk
git clone https://bitbucket.org/JobLeadsGmbH/zendesk.git .
```

## By [Composer](http://getcomposer.org)

* adapt your composer.json

```
    "repositories": [
        {
          "url": "https://bitbucket.org/jobleadsgmbh/zendesk",
          "type": "vcs"
        }
    ],
    "require": {
        "jobleadsgmbh/zendesk": "dev-master"
    }
```

# Configuration

This component comes with a "zendesk.global.php.dist" example configuration.
Copy and paste this file in '<project root>/config/autoload/zendesk.global.php' and replace the '<place holders>' with something useful.

# History

* upcomming
    * @todo
        * fixed bug in installation by composer tutorial
        * move code to [jobleads](https://github.com/jobleads)
    * added comment how to generate "value" in "zendesk.global.php"
* [v1.2.0](https://bitbucket.org/JobLeadsGmbH/zendesk/?at=v1.2.0) - released at 2015-06-02
    * fixed [zendesk client](https://github.com/zendesk/zendesk_api_client_php) dependency to [v1.2.0](https://github.com/zendesk/zendesk_api_client_php/tree/v1.2.0)
* [v1.1.0](https://bitbucket.org/JobLeadsGmbH/zendesk/?at=v1.1.0) - released at 2015-06-02
    * fixed broken link in README.md
    * removed dependency to phpunit (since no test is available)
* [v1.0.1](https://bitbucket.org/JobLeadsGmbH/zendesk/?at=v1.0.1) - released at 2015-06-02
    * updated / created README.md
    * extended zf2 version requirement to "2.\*"
* [v1.0](https://bitbucket.org/JobLeadsGmbH/zendesk/?at=v1.0) - released at 2014-12-08
    * created by [ndevr/zendesk](https://bitbucket.org/ndevr/zendesk) for JobLeads GmbH
