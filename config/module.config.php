<?php

return [
    'zendesk' => [
        'subdomain' => '',
        'username' => '',
        'auth' => ['method' => 'token', 'value' => '']
    ],
    'service_manager' => [
        'factories' => [
            Zendesk\API\Client::class => ZendeskApi\Factory\ZendeskApiClientFactory::class
        ]
    ]
];