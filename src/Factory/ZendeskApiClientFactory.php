<?php

declare(strict_types=1);

namespace ZendeskApi\Factory;

use Psr\Container\ContainerInterface;
use Zendesk\API\Client;

class ZendeskApiClientFactory
{
    public function __invoke(ContainerInterface $container): Client
    {
        $config = $container->get('Configuration');

        $client = new Client($config['zendesk']['subdomain']);
        $client->setAuth('basic', [
            'username' => $config['zendesk']['username'],
            'token' => $config['zendesk']['auth']['value']
        ]);

        return $client;
    }
}
